﻿using UnityEngine;
using UnityEngine.UI;

/*
 * Class used to handle the debug menu
 * The debug menu is activated using the Space key. The reason for that is because I don't have a UK layout keyboard so I chose a key that is the same in every layout
 * */
public class DebugMenu : MonoBehaviour
{
    public GameObject debugCanvas;
    public Slider itemSlider;
    public Slider RecipeSlider;

    void Start()
    {
        itemSlider.value = Game.ActiveGame.getItemDensity();
        RecipeSlider.value = Game.ActiveGame.getRecipeRange();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            debugCanvas.SetActive(!debugCanvas.activeSelf);
    }

    public void setItemDensity()
    {
        if (debugCanvas.activeSelf)
            Game.ActiveGame.setItemDensity(itemSlider.value);
    }

    public void setRecipeRange()
    {
        if (debugCanvas.activeSelf)
            Game.ActiveGame.setRecipeRange(RecipeSlider.value);
    }
}
