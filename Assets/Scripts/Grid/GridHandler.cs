﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
 * Refactored class to have a dynamic grid size
 * The grid size has a max value to prevent it from going out of the screen, while the min value is 1
 * Moved the code that creates the grid in Awake, and had MixingGridHandler override it so that the generation code won't be calling by it, but only by GridHandler
 */

public class GridHandler : MonoBehaviour
{
	#region fields

	//temp until refactor generation code
	private List<GameObject> _rows = new List<GameObject>();

	protected List<GridCell> _emptyCells = new List<GridCell>();
	protected List<GridCell> _fullCells = new List<GridCell>();

	public Transform parentObject;
	public GridCell prefabCell;

	[SerializeField] private int _gridX = 6;
	[SerializeField] private int _gridY = 5;

	[SerializeField] private Vector3 _startPosition = new Vector3(-2.68f, 1.84f, 0);
	[SerializeField] private float _deltaXPosition = 1.04f;
	[SerializeField] private float _deltaYPosition = -1;


	public List<GridCell> GetFullCells => _fullCells;
	public List<GridCell> GetEmptyCells => _emptyCells;

	#endregion

	#region initialization

	protected virtual void Awake()
	{
		ClearExistingCells();

		CreateNewGrid();

		// Using the same code from before to set the neighbours
		for (int i = 0; i < _rows.Count; i++)
		{
			//each row
			List<GridCell> currentRow = _rows[i].GetComponentsInChildren<GridCell>().ToList();
			List<GridCell> upperRow = i > 0 ? _rows[i - 1].GetComponentsInChildren<GridCell>().ToList() : null;
			List<GridCell> lowerRow = i + 1 < _rows.Count ? _rows[i + 1].GetComponentsInChildren<GridCell>().ToList() : null;

			for (int j = 0; j < currentRow.Count; j++)
			{
				//each cell
				currentRow[j].SetNeighbor(upperRow?[j], MoveDirection.Up);
				currentRow[j].SetNeighbor(lowerRow?[j], MoveDirection.Down);

				var leftN = j > 0 ? currentRow[j - 1] : null;
				currentRow[j].SetNeighbor(leftN, MoveDirection.Left);

				var rightN = j < currentRow.Count - 1 ? currentRow?[j + 1] : null;
				currentRow[j].SetNeighbor(rightN, MoveDirection.Right);
				currentRow[j].SetHandler(this);
				
				//cache the cell as empty
				_emptyCells.Add(currentRow[j]);
			}
		}
	}

	// Creates a grid based on the values x/y provided
	private void CreateNewGrid()
    {
		if (PlayerPrefs.HasKey("GridX"))
			_gridX = PlayerPrefs.GetInt("GridX");
		if (PlayerPrefs.HasKey("GridY"))
			_gridY = PlayerPrefs.GetInt("GridY");

		for (int i = 0; i < _gridY; i++)
		{
			var newRow = new GameObject("NewRow" + i);
			newRow.transform.SetParent(parentObject);

			for (int j = 0; j < _gridX; j++)
			{
				Vector3 position = new Vector3(_startPosition.x + _deltaXPosition * j, _startPosition.y + _deltaYPosition * i);
				Instantiate(prefabCell, position, Quaternion.identity, newRow.transform);
			}

			_rows.Add(newRow);
		}
	}

	private void ClearExistingCells()
	{
		_emptyCells = new List<GridCell>();
		_fullCells = new List<GridCell>();
	}

	#endregion

	#region helpers

	public void AddMergeableItemToEmpty(MergableItem item)
	{
		var cell = _emptyCells.FirstOrDefault();
		if (cell != null)
		{
			item.AssignToCell(cell);
		}
	}

	public void ClearCell(GridCell cell)
	{
		if (_fullCells.Contains(cell))
		{
			_fullCells.Remove(cell);
			cell.ClearItem();
		}
		
		if (!_emptyCells.Contains(cell))
			_emptyCells.Add(cell);
	}


	public void SetCellState(GridCell cell, bool empty)
	{
		if (cell == null) return;
		if (!empty)
		{
			if (_fullCells.Contains(cell))
			{
				_fullCells.Remove(cell);
			}

			if (_emptyCells.Contains(cell) == false)
			{
				_emptyCells.Add(cell);
			}
		}
		else
		{
			if (_emptyCells.Contains(cell))
			{
				_emptyCells.Remove(cell);
			}

			if (_fullCells.Contains(cell) == false)
			{
				_fullCells.Add(cell);
			}
		}
	}

	#endregion
}
