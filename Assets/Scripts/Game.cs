﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using GramGames.CraftingSystem.DataContainers;

/*
 * Made this class into a singleton and fixed item generation to use a range of recipes and have an item density
 * */

public class Game : MonoBehaviour
{
	public static Game ActiveGame { get; private set; }

	public MergableItem DraggableObjectPrefab;
	public GridHandler MainGrid;
	private int recipeLenght = ItemUtils.RecipeMap.Count;
	[SerializeField] [Range(0, 100)] private float _itemDensity = 100;
	[SerializeField] [Range(1, 25)] private float _recipeRange = 1;

	private List<string> ActiveRecipes = new List<string>();

	private void Awake()
	{
		if (ActiveGame == null)
		{
			ActiveGame = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			// Set the load order of Game to be lower, so the cells should be already be set up when we reach this point
			ActiveGame.MainGrid = MainGrid;
			ActiveGame.ReloadLevel();
			Destroy(gameObject);
		}
	}

	private void Start()
	{
		Screen.fullScreen =
			false; // https://issuetracker.unity3d.com/issues/game-is-not-built-in-windowed-mode-when-changing-the-build-settings-from-exclusive-fullscreen

		// load all item definitions
		ItemUtils.InitializeMap();

		ReloadLevel();
	}

	public void ReloadLevel()
	{
		// clear the board
		var fullCells = MainGrid.GetFullCells.ToArray();
		for (int i = fullCells.Length - 1; i >= 0; i--)
			MainGrid.ClearCell(fullCells[i]);

		// choose new recipes
		ActiveRecipes.Clear();
		var difficulty = Mathf.Clamp(_recipeRange, 1, ItemUtils.RecipeMap.Count);
		var recipes = new Dictionary<string, HashSet<NodeData>>(ItemUtils.RecipeMap);

		for (int i = 0; i < difficulty; i++)
		{
			// a 'recipe' has more than 1 ingredient, else it is just a raw ingredient.
			var recipe = recipes.FirstOrDefault(kvp => kvp.Value.Count > 1).Key;
			recipes.Remove(recipe);
			ActiveRecipes.Add(recipe);
		}

		// populate the board
		var emptyCells = MainGrid.GetEmptyCells.ToArray();
		foreach (var cell in emptyCells)
		{
			if (Random.Range(1, 100) > _itemDensity)
				continue;

			var chosenRecipe = ActiveRecipes[Random.Range(0, ActiveRecipes.Count)];
			var ingredients = ItemUtils.RecipeMap[chosenRecipe].ToArray();
			var ingredient = ingredients[Random.Range(0, ingredients.Length)];
			var item = ItemUtils.ItemsMap[ingredient.NodeGUID];
			cell.SpawnItem(item);
		}
	}

	public float getItemDensity() { return _itemDensity; }

	public float getRecipeRange() { return _recipeRange; }

	public void setItemDensity(float value) { _itemDensity = value; }

	public void setRecipeRange(float value) { _recipeRange = value; }

	public static void Activate(bool active)
    {
		if (ActiveGame != null)
			ActiveGame.gameObject.SetActive(active);
    }

}
