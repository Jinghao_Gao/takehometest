﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene("Game");
        Game.Activate(true);
    }

    public void ExitToMenu()
    {
        SceneManager.LoadScene("Menu");
        Game.Activate(false);
    }

    public void EnableSettings(bool enable)
    {
        Settings.ActiveSettings.EnableSettings(enable);
    }
}
