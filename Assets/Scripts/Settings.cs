﻿using UnityEngine.UI;
using UnityEngine;

/**
 * Added this class to handle the settings menu and the various slide bars
 * This is also a singleton, a shared settings menu accessible from both the game and the main menu
 */

public class Settings : MonoBehaviour
{
    public static Settings ActiveSettings { get; private set; }

    public GameObject SettingCanvas;
    public Slider VolumeSlider;
    public Slider GridSliderX;
    public Slider GridSliderY;

    private float _volume = 1;
    private int _gridSizeX = 6;
    private int _gridSizeY = 5;

    private readonly string _volumeID = "Volume";
    private readonly string _gridXID = "GridX";
    private readonly string _gridYID = "GridY";

    private void Awake()
    {
        if (ActiveSettings == null)
        {
            ActiveSettings = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    // Loads the values if the were saved before
    private void Start()
    {
        if (PlayerPrefs.HasKey(_volumeID))
            _volume = PlayerPrefs.GetFloat(_volumeID);
        if (PlayerPrefs.HasKey(_gridXID))
            _gridSizeX = PlayerPrefs.GetInt(_gridXID);
        if (PlayerPrefs.HasKey(_gridYID))
            _gridSizeY = PlayerPrefs.GetInt(_gridYID);

        VolumeSlider.value = _volume;
        GridSliderX.value = _gridSizeX;
        GridSliderY.value = _gridSizeY;
    }

    public void UpdateVolume(Text textObject)
    {
        _volume = VolumeSlider.value;
        PlayerPrefs.SetFloat(_volumeID, _volume);
        textObject.text = _volume.ToString();
    }

    public void UpdateGridX(Text textObject)
    {
        _gridSizeX = (int)GridSliderX.value;
        PlayerPrefs.SetInt(_gridXID, _gridSizeX);
        textObject.text = _gridSizeX.ToString();
    }

    public void UpdateGridY(Text textObject)
    {
        _gridSizeY = (int)GridSliderY.value;
        PlayerPrefs.SetInt(_gridYID, _gridSizeY);
        textObject.text = _gridSizeY.ToString();
    }

    public void EnableSettings(bool enable)
    {
        SettingCanvas.SetActive(enable);
    }
}
