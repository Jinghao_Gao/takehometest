Hi,
for this project I added 3 additional classes

DebugMenu
Menu
Settings

And modified mainly the classes

Game
GridHandler

Some other classes were slightly modified, but they weren't anything major.

The only thing to note is that if you open the scene "Game" and try to open the settings menu, it will give you an error. That's because the settings object is in the "Menu" scene.
That shouldn't be a real issue as the game is meant to be accessed only from the menu.